module se09.com

go 1.15

require (
	github.com/bmizerany/pat v0.0.0-20170815010413-6226ea591a40
	github.com/coreos/go-systemd v0.0.0-20190719114852-fd7a80b32e1f // indirect
	github.com/golangcollege/sessions v1.2.0
	github.com/jackc/pgx/v4 v4.10.1
	github.com/justinas/alice v1.2.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/text v0.3.4 // indirect
	golang.org/x/tools v0.0.0-20191029190741-b9c20aec41a5 // indirect
)
